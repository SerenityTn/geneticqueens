package main;

import java.util.ArrayList;

public class Tree {	
    Integer racine;    
    public ArrayList<Integer> avPos = new ArrayList<Integer>();
    public Tree childs[];        
    public Tree(Integer[] chromo, int index, ArrayList<Integer> avPos){         	
    	ArrayList<Integer> aPos = new ArrayList<Integer>(avPos);
    	childs = new Tree[chromo.length];
    	this.racine = chromo[index];    	
    	aPos.remove(this.racine);
    	if(index < chromo.length-1){    		
    		this.childs[chromo[index+1]] = new Tree(chromo, index+1, aPos);
    	}
    }        
} 


