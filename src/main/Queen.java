package main;

import gui.ChessBoard;
import csp.Constraints;

public class Queen {
	public static int[] calcViolConst(Integer chromo[], int sigma, int valGene){
		Integer[] newChromo = new Integer[chromo.length];
		int[] newTemplate = new int[chromo.length];							
		System.arraycopy(chromo, 0, newChromo, 0, chromo.length );	
		newChromo[sigma] = valGene;
		for(int i = 0; i < ChessBoard.size; i++){
			for(int j = i+1; j < ChessBoard.size; j++){				
				int ncv = Constraints.checkQueens(i,j,newChromo[i],newChromo[j]);			
				newTemplate[i] += ncv;
				newTemplate[j] += ncv;								
			}
		}	
		return newTemplate;		
	}
}
