package main;
import java.util.ArrayList;
import java.util.Arrays;

import csp.Constraints;
import gui.ChessBoard;

public class Individu {
	public Integer [] chromo;
	public int [] template;
	public int fitness;	
	public int maxIndex;
	public static Individu bestChromo;			
	
	public Individu(Individu newIdv){
		chromo = new Integer[ChessBoard.size];
		template  = new int[ChessBoard.size];
		for(int i = 0; i < ChessBoard.size; i++){
			chromo[i] = newIdv.chromo[i];
			template[i] = newIdv.template[i];						
		}
		calcTemplate();
	}
			
	public Individu(Boolean x){
		ArrayList<Integer> avPos = new ArrayList<Integer>();
		for(int i = 0; i < ChessBoard.size; i++) avPos.add(i);		
		chromo = new Integer[ChessBoard.size];
		template  = new int[ChessBoard.size];
		for(int i = 0; i < ChessBoard.size; i++){
			int index = (int) (Math.random()*avPos.size());			
			chromo[i] = avPos.get(index);
			avPos.remove(index);
		}
		calcTemplate();
	}
		
	public Individu(){
		chromo = new Integer[ChessBoard.size];
		template  = new int[ChessBoard.size];
	}
	
	public int calcTemplate(){		
		Arrays.fill(template, 0);//remplir le tableau template avec des zeros
		for(int i = 0; i < ChessBoard.size; i++){
			for(int j = i+1; j < ChessBoard.size; j++){				
				int ncv = Constraints.checkQueens(i,j,chromo[i],chromo[j]);				
				template[i] += ncv;
				template[j] += ncv;													
			}
		}	
		updateMaxInfo();		
		if(bestChromo != null){
			if(this.fitness < bestChromo.fitness) bestChromo = this;
		}else{
			bestChromo = this;			
		}
		return maxIndex;
	}
	
	void updateMaxInfo(){
		int mx = 0;
		for(int i = 0; i < template.length; i++){
			if(template[i] > mx){
				mx = template[i];
				maxIndex = i;
			}
		}
		this.fitness = mx;
	}	
	
	public String toString(){
		String msg = "";
		for(int i = 0; i < chromo.length; i++){
			msg += chromo[i] + " ";
		}
		return msg;
	}
	
	public void showTemplate(){				
		for(int i = 0; i < ChessBoard.size; i++){			
			System.out.print(template[i]+" ");
		}		
		System.out.println(this.fitness);
	}
}
