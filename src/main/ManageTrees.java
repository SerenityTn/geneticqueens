package main;
import gui.ChessBoard;

import java.util.ArrayList;



class PosList extends ArrayList<Integer> {

}

public class ManageTrees {
	public  int SIZE = ChessBoard.size;
    public Tree trees[];    
    
    public ManageTrees(){
    	trees = new Tree[SIZE]; 
    }
    
    public boolean addElement(Integer[] chromo, boolean canModify){    	
    	if(trees[chromo[0]] == null){    		
    		ArrayList<Integer> avPos = new ArrayList<Integer>();
    		for(int i = 0; i < chromo.length; i++) avPos.add(i);
    		Tree t = new Tree(chromo, 0, avPos);
    		trees[chromo[0]] = t;
    		return true;
    	}else{    		
    		Tree initialTree = trees[chromo[0]];
    		if(!canModify){
    			return addPartChromo(initialTree, chromo, 1);
    		}else{
    			ArrayList<Integer> avPos = new ArrayList<Integer>();
    			for(int i = 0; i < ChessBoard.size; i++) avPos.add(i);
    			avPos.remove(chromo[0]);
    			return addAndModify(initialTree, chromo, avPos, 1);
    		}
    	}
    }
    
	private boolean addPartChromo(Tree t, Integer[] chromo, int index){
		if(index == chromo.length-1) return false;
		else if(t.childs[chromo[index]] != null){
			return addPartChromo(t.childs[chromo[index]], chromo, index+1);
		}else{				
			t.childs[chromo[index]] = new Tree(chromo, index, t.avPos);
			return true;
		}    	    
	}
	
	private boolean addAndModify(Tree t, Integer[] chromo, ArrayList<Integer> avPos, int index){
		if(index == chromo.length-1) return false;
		else if(t.childs[chromo[index]] != null){
			return addPartChromo(t.childs[chromo[index]], chromo, index+1);
		}else{				
			t.childs[chromo[index]] = new Tree(chromo, index, t.avPos);
			return true;
		} 	
	}
	
}
