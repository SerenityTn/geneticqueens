package main;

import java.io.IOException;
import java.util.ArrayList;

import gui.ChessBoard;
import utils.ExterneOutput;

public class GeneticAlgorithm {
	public int iterations;
	public int[] solutionChromo = new int[ChessBoard.size];
	static ArrayList<Individu> GeneratePopulation(Population pop, int currentGen) throws IOException{
		String genName = "Generation " + currentGen;
		ExterneOutput.makeDirectory(genName);

		ExterneOutput.writeGenerationtData(currentGen, "_InitialPop", pop.initialPop);
		//selection
		ArrayList<Individu> selectedPop = pop.tourSelection();
		ExterneOutput.writeGenerationtData(currentGen, "__SelectedPop", selectedPop);
		//crossover
		ArrayList<Individu> crossedPop = pop.crossingOver(selectedPop, currentGen);
		ExterneOutput.writeGenerationtData(currentGen, "____CrossedPop", crossedPop);
		//mutation
		ArrayList<Individu> mutatedPop =  pop.mutation(crossedPop);
		ExterneOutput.writeGenerationtData(currentGen, "_____MutatedPop", mutatedPop);
		//replacement
		ArrayList<Individu> replacedPop  = pop.replacement(mutatedPop, selectedPop);
		ExterneOutput.writeGenerationtData(currentGen, "______ReplacedPop", replacedPop);
		//bestchromo
		Individu.bestChromo = replacedPop.get(0);
		ExterneOutput.writeBestChromo(currentGen, "BestChromosom", Individu.bestChromo);

		Population.randomizePopulation(replacedPop);
		return replacedPop;
	}

	public GeneticAlgorithm(int iterations) throws IOException{
		this.iterations = iterations;
		System.out.println("Population size : "+Population.popSize);
		System.out.println("Selection probability : "+Population.selectionProb);
		System.out.println("Crossover probability : "+Population.crossProb);
		System.out.println("Mutation probability : "+Population.mutationProb);
		Population pop = new Population();
		int count = 0;
		do{
			ArrayList finalPop = GeneratePopulation(pop, count+1);
			pop = new Population(finalPop);	//envoie la population i-1 dans le contructeur de nouvel population
			count++;
		}while(count+1 <= iterations);
	}
}
