package main;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Random;
import gui.ChessBoard;
import utils.ExterneOutput;

public class Population {
	static public int popSize;
	static public double mutationProb;//pour que tous les instances de population utilisent le meme valeur de probabilité
	static public double crossProb;
	static public double selectionProb;
	ArrayList<Individu> initialPop;

	public Population(){
		createPop();
	}

	public Population(ArrayList<Individu> newPopulation){
		initialPop = new ArrayList<Individu>(newPopulation);
	}

	/* 0 1 2 3 !0
	 * 2
	 * 0 1 3 !1
	 * 1
	 * 0 3 !2
	 * 3
	 * 0 !3
	 * 0
	 */

	private void generatePop(ArrayList<Individu> pop, ArrayList<Integer> avPos, Individu idv, int index){
		if(pop.size() != Population.popSize){
			if(avPos.size() == 0){
				Individu newIdv = new Individu(idv);
				pop.add(newIdv);
			}else{
				ArrayList<Integer> aPos = new ArrayList<Integer>(avPos);
				while(aPos.size() != 0){
					int pos = (int)(Math.random()*aPos.size());
					idv.chromo[index] = aPos.get(pos);
					avPos.remove(idv.chromo[index]);
					aPos.remove(idv.chromo[index]);
					generatePop(pop, avPos, idv, index+1);
					avPos.add(idv.chromo[index]);
				}
			}
		}
	}

	public void createPop(){
		initialPop = new ArrayList<Individu>();
		if(ChessBoard.size <= 6){
			ArrayList<Integer> avPos = new ArrayList<Integer>();
			for(int i = 0; i < ChessBoard.size; i++) avPos.add(i);
			Individu idv = new Individu();
			generatePop(initialPop, avPos, idv, 0);
			Population.randomizePopulation(initialPop);
		}else{
			for(int i = 0; i < popSize; i++){
				boolean b = false;
				Individu idv = null;
				ManageTrees m = new ManageTrees();
				do{
					idv = new Individu(true);
				}while(!m.addElement(idv.chromo, false));
				initialPop.add(idv);
			}
		}
		for(int i = 0; i < 5; i++){
			Individu x = new Individu();
			Integer[] chromo = {0,1,2,3,4,5,6,7} ;
			x.chromo = chromo;
		}
	}

	Individu maxChromo(Individu o, Individu o1){
		return o.fitness < o1.fitness ? o:o1;
	}


	public Individu select(int k, ArrayList<Individu> tempPop){
		Individu best = null;
		for(int i = 0; i < k; i++){
			Individu rndIndv = getRndChromo(tempPop);
	        if (best == null || rndIndv.fitness < best.fitness) best = rndIndv;
		}
		return best;
	}

	ArrayList<Individu> tourSelection(){
		ArrayList<Individu> selection = new ArrayList<Individu>();
		ArrayList<Individu> tempPop = new ArrayList<Individu>(initialPop);
		for(int i = 0; i < initialPop.size(); i++){
			double pb = Math.random();
			if(pb <= selectionProb){
				Individu bestOne = select(10, initialPop);
				tempPop.remove(bestOne);
				selection.add(bestOne);
			}
		}
		randomizePopulation(selection);
		return selection;
	}

	ArrayList<Individu[]> make_pairs(ArrayList<Individu> pop){
		ArrayList<Individu[]> parentsPop = new ArrayList<Individu[]>();
		ArrayList<Individu> popTemp = new ArrayList<Individu>(pop);
		while(popTemp.size() >= 2){
			Individu[] pair = new Individu[2];
			Individu c1 = getRndChromo(popTemp);
			pair[0] = c1;
			popTemp.remove(c1);
			Individu c2 = getRndChromo(popTemp);
			popTemp.remove(c2);
			pair[1] = c2;
			parentsPop.add(pair);
		}
		return parentsPop;
	}

	Individu cross(Individu father, Individu mother){
		int sum;
		Individu offSpring = new Individu();
		/*int x = randomValue(0, mother.chromo.length);
		for(int i = 0; i < mother.chromo.length; i++){
			if(i < x) offSpring.chromo[i] = mother.chromo[i];
			else offSpring.chromo[i] = father.chromo[i];
		}*/
		for(int i = 0; i < father.chromo.length; i++){
			sum = father.template[i] + mother.template[i]; // 5 2 = 7
			if(randomValue(0, sum-1) < father.template[i]) // 0..7 = 4
				offSpring.chromo[i] = mother.chromo[i];
			else
				offSpring.chromo[i] = father.chromo[i];
		}
		return offSpring;
	}

	ArrayList<Individu> crossingOver(ArrayList<Individu> selectedPop, int genIndex) throws IOException{
		ArrayList<Individu[]> matingPool = make_pairs(selectedPop);
		try {
			ExterneOutput.writeMatingPool(genIndex, "___Mating pool", matingPool);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ArrayList<Individu> offSpringPop = new ArrayList<Individu>();
		for(int i = 0; i < matingPool.size(); i++){
			double pb = Math.random();
			if(pb <= crossProb){
				Individu[] parents = matingPool.get(i);
				Individu offSpring = cross(parents[0], parents[1]);
				offSpring.calcTemplate();
				offSpringPop.add(offSpring);
			}
		}
		return offSpringPop;
	}

	void heur_min_confli(Individu offSpring){
		int sigma = offSpring.maxIndex;
		int min_nvc =  ChessBoard.size - Math.abs(offSpring.chromo[sigma] - sigma);
		for(int i = 0; i < ChessBoard.size; i++){
			if(i != sigma){
				int[] newTemplate = Queen.calcViolConst(offSpring.chromo, sigma, i);
				int nvc = newTemplate[sigma];
				if(nvc < min_nvc){
					min_nvc = nvc;
					offSpring.chromo[sigma] = i;
					offSpring.template = newTemplate;
					offSpring.updateMaxInfo();
				}
			}
		}
	}

	ArrayList<Individu> mutation(ArrayList<Individu> offSpringPop){
		ArrayList<Individu> mutatedPop = new ArrayList<Individu>();

		for(int i = 0; i < offSpringPop.size(); i++){
			double pb = Math.random();
			Individu offSpring =  offSpringPop.get(i);
			if(pb <= mutationProb){
				heur_min_confli(offSpring);
			}

			mutatedPop.add(offSpring);
		}
		return mutatedPop;
	}

	ArrayList<Individu> replacement(ArrayList<Individu>  mutatedPop, ArrayList<Individu> selectedPop){
		mutatedPop.addAll(selectedPop);
		ManageTrees m = new ManageTrees();
		for(int i = 0; i < mutatedPop.size(); i++){
			if(!m.addElement(mutatedPop.get(i).chromo, false)){
				mutatedPop.remove(i);
			}
		}
		Collections.sort(mutatedPop, new Comparator<Individu>() {
	        @Override public int compare(Individu o1, Individu o2) {
	            return o1.fitness - o2.fitness; // Ascending
	        }
	    });
		if(mutatedPop.size() > Population.popSize){
			ArrayList<Individu> replacedPop = new ArrayList<Individu>(mutatedPop.subList(0, Population.popSize));
			return replacedPop;
		}else{
			return mutatedPop;
		}
	}

	static void writePopulation(PrintWriter file, ArrayList<Individu> a){
		file.println("  Chromosoms  \t            Template  \t        Fitness");
		for(int i = 0; i < a.size(); i ++){
			Individu temp = a.get(i);
			for(int j = 0; j < temp.chromo.length; j++) file.print(temp.chromo[j]+" ");
			file.print("\t");
			for(int j = 0; j < temp.chromo.length; j++) file.print(temp.template[j]+" ");
			file.println("\t"+temp.fitness);
		}
	}

	static void writeBestChromo(PrintWriter file, Individu bestChromo){
		file.println("Chromosoms \t      Template\t      Fitness");
		for(int i = 0; i < bestChromo.chromo.length; i++) file.print(bestChromo.chromo[i]+" ");
		file.print("\t");
		for(int i = 0; i < bestChromo.chromo.length; i++) file.print(bestChromo.template[i]+" ");
		file.println("\t"+bestChromo.fitness);

	}

	Individu getRndChromo(ArrayList<Individu> L){
		return L.get((int)(Math.random()*L.size()));
	}

	int randomValue(int min, int max){
	   int range = (max - min) + 1;
	   return (int)(Math.random() * range) + min;
	}

	static void randomizePopulation(ArrayList<Individu> L){
		long seed = System.nanoTime();
		Collections.shuffle(L, new Random(seed));
	}

	int min(int a, int b){
		if(a < b) return a;
		else return b;
	}
}
