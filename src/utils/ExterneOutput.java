package utils;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import main.Individu;

public class ExterneOutput {
	public final static Path geneticPath = Paths.get(System.getProperty("user.home")).resolve("Desktop").resolve("GeneticOutput");


	public static void writeExecDetails(int execIndex, long executionTime, int fitness) throws IOException{
		PrintWriter file = ExterneOutput.newWriter("Execution.txt", true);
		file.println();
		file.println("Exectuion "+execIndex+":");
		file.println("Execution time "+ executionTime);
		file.println("Fitness "+ fitness);
		file.close();
	}

	public static Path getPath(){
		return geneticPath.resolve("GeneticAnalysis").resolve("Execution");
	}

	public static void writePopulation(PrintWriter file, ArrayList<Individu> a){
		file.println("  Chromosoms  \t            Template  \t        Fitness");
		for(int i = 0; i < a.size(); i ++){
			Individu temp = a.get(i);
			for(int j = 0; j < temp.chromo.length; j++) file.print(temp.chromo[j]+" ");
			file.print("\t");
			for(int j = 0; j < temp.chromo.length; j++) file.print(temp.template[j]+" ");
			file.println("\t"+temp.fitness);
		}
	}

	static String getPath(int genIndex){
		return "Generation " + genIndex + "/";
	}

	public static void writeBestChromo(int genIndex, String popName, Individu bestChromo) throws IOException{
		PrintWriter file = ExterneOutput.newWriter(getPath(genIndex) + popName + ".txt", false);
		file.println("Chromosoms \t      Template\t      Fitness");
		for(int i = 0; i < bestChromo.chromo.length; i++) file.print(bestChromo.chromo[i]+" ");
		file.print("\t");
		for(int i = 0; i < bestChromo.chromo.length; i++) file.print(bestChromo.template[i]+" ");
		file.println("\t"+bestChromo.fitness);
		file.close();
	}

	public static void makeDirectory(String name){
		File dir = geneticPath.resolve(name).toFile();
		dir.mkdir();
	}

	public static void writeGenerationtData(int genIndex, String popName, ArrayList<Individu> pop) throws IOException{
		PrintWriter file = ExterneOutput.newWriter(getPath(genIndex) + popName + ".txt", false);
		file.println("Population size : "+ pop.size());
		file.println();
		file.println("  Chromosoms  \t            Template  \t        Fitness");
		for(Individu temp : pop){
			for(int j = 0; j < temp.chromo.length; j++) file.print(temp.chromo[j]+" ");
			file.print("\t");
			for(int j = 0; j < temp.chromo.length; j++) file.print(temp.template[j]+" ");
			file.println("\t"+temp.fitness);
		}
		file.close();
	}

	public static void writeMatingPool(int genIndex, String popName, ArrayList<Individu[]> pop) throws IOException{
		PrintWriter file = ExterneOutput.newWriter(getPath(genIndex) + popName + ".txt", false);
		file.println("  Chromosoms  \t            Template  \t        Fitness");
		for(Individu[] temp : pop){
			for(int j = 0; j < temp[0].chromo.length; j++) file.print(temp[0].chromo[j]+" ");
			file.print("\t");
			for(int j = 0; j < temp[0].chromo.length; j++) file.print(temp[0].template[j]+" ");
			file.println("\t"+temp[0].fitness);
			for(int j = 0; j < temp[0].chromo.length; j++) file.print(temp[1].chromo[j]+" ");
			file.print("\t");
			for(int j = 0; j < temp[0].chromo.length; j++) file.print(temp[1].template[j]+" ");
			file.println("\t"+temp[1].fitness);
			file.println();
		}
		file.close();
	}

	public static PrintWriter newWriter(String name, boolean append) throws IOException{
		PrintWriter popWriter = null;
		try {
			if(!append) popWriter = new PrintWriter(geneticPath.resolve(name).toFile(), "UTF-8");
			else popWriter = new PrintWriter(new BufferedWriter(new FileWriter(geneticPath.resolve(name).toFile(), true)));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return popWriter;
	}
}
