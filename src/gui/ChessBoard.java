package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.util.Scanner;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;

public class ChessBoard {
	public static int size = 8;
	public static JButton [][] boardMatrix;
	public static JPanel boardPanel;
	static final int BoardDimension =  400;
	static final int Margin = 40;
	static ImageIcon icon = new ImageIcon("queen2D.png");	
	
	static Image img = icon.getImage() ;  
	static Image newimg;
	static ImageIcon queenIcon;
	
	
	
	public static void initBoard(){
		newimg = img.getScaledInstance((BoardDimension-Margin)/size, (BoardDimension-Margin)/size, java.awt.Image.SCALE_SMOOTH);
		queenIcon = new ImageIcon(newimg);
		boardMatrix = new JButton[size][size];
		boardPanel = new JPanel(new GridLayout(size, size));
		boardPanel.setPreferredSize(new Dimension(BoardDimension, BoardDimension));
		JButton btn;        		
		for(int i = 0; i < size; i++){									
			for(int j = 0; j < size; j++){	
				btn = new JButton();
				boardMatrix[i][j] = btn;			
				if((i+1+j)%2 == 0) btn.setBackground(Color.BLACK);				
				boardPanel.add(btn);				
			}			
		} 				
	}
	
	public static void placeQueens(Integer[] QueenSetup){		
		if(QueenSetup != null){
			for(int i = 0; i < QueenSetup.length; i++)
				for(int j = 0; j < QueenSetup.length; j++)
					boardMatrix[i][j].setIcon(null);
			
			for(int i = 0; i < QueenSetup.length; i++){			
				System.out.println(i+" "+QueenSetup[i]);
				boardMatrix[i][QueenSetup[i]].setIcon(queenIcon);
			}
		}
	}
}
