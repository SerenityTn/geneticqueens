package gui;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import main.GeneticAlgorithm;
import main.Individu;
import main.Population;
import utils.ExterneOutput;


public class AlgorithmInterface {
	static JFrame frame;
	public static final int WIDTH = 805, HEIGHT = WIDTH/4 * 3;// declaration du taille de la fenetre
	public static void main(String[] args) {
		System.out.println("testing...");
		Individu test = new Individu();
		Integer[] chromo = {1,3,4,7,2,6,1,2};
		test.chromo = chromo;
		test.calcTemplate();
		test.showTemplate();
		//Creation de la fen�tre
		frame = new JFrame("N Queens Genetic Algorithm solution");
		frame.setPreferredSize(new Dimension(WIDTH, HEIGHT));
		frame.setMaximumSize(new Dimension(WIDTH, HEIGHT));
		frame.setMinimumSize(new Dimension(WIDTH, HEIGHT));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		System.out.println("Openning window !");


		ChessBoard.initBoard();

		String[] menuGui = {"chessboard size", "population size", "selection prob", "crossover prob", "mutation prob",
							"iterations count"};

		//Cr�ation de 4 JPanels
		//final JPanel mainPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		final JPanel mainPanel = new JPanel(new GridLayout(1,2));
		final JPanel configPanel  = new JPanel(new FlowLayout());
		final JPanel resPanel = new JPanel(new FlowLayout());
		final JPanel paramPanel = new JPanel(new GridBagLayout());
		GridBagConstraints gc = new GridBagConstraints();
		gc.gridx = 0;
		gc.gridy = 0;
		final JPanel solPanel = new JPanel(new GridLayout(2, ChessBoard.size));

		//Cr�ation JLabels

		//Cr�ation de JTextbox
		final JTextField[] fieldRef = new JTextField[menuGui.length];

		//Cr�ation de JButton
		JButton runAlgoBtn = new JButton("Execute genetic algorithm");

		final JLabel nvcLbl = new JLabel("Number of violated Constraints");
		final JButton fitnessBtn = new JButton("/");
		for(int i = 0; i < ChessBoard.size; i++) solPanel.add(new JLabel(i+"", SwingConstants.CENTER));
		for(int i = 0; i < ChessBoard.size; i++) solPanel.add(new JButton("0"));
		//Execute Button Listener
		runAlgoBtn.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){

				ChessBoard.size          = Integer.parseInt(fieldRef[0].getText());
				Population.popSize       = Integer.parseInt(fieldRef[1].getText());
				Population.selectionProb = Double.parseDouble(fieldRef[2].getText());
				Population.crossProb     = Double.parseDouble(fieldRef[3].getText());
				Population.mutationProb  = Double.parseDouble(fieldRef[4].getText());
				int iterations = Integer.parseInt(fieldRef[5].getText());


				/*ChessBoard.size          = 12;
				Population.popSize       = 100000;
				Population.selectionProb = 0.5;
				Population.crossProb     = 0.4;
				Population.mutationProb  = 0.2;

				int iterations = 5;*/

				resPanel.remove(ChessBoard.boardPanel);
				ChessBoard.initBoard();
				resPanel.add(ChessBoard.boardPanel);
				GeneticAlgorithm G = null;
				PrintWriter file;

				try {
					file = new PrintWriter(ExterneOutput.geneticPath.resolve("Execution.txt").toFile(), "UTF-8");
					file.close();
				} catch (FileNotFoundException e2) {
					e2.printStackTrace();
				} catch (UnsupportedEncodingException e2) {
					e2.printStackTrace();
				}

				for(int k = 1; k <= 1; k++){
					Individu.bestChromo = new Individu();
					try {
						long startTime = System.currentTimeMillis();
						G = new GeneticAlgorithm(iterations);
						long stopTime = System.currentTimeMillis();
						long elapsedTime = stopTime - startTime;
						ExterneOutput.writeExecDetails(k, elapsedTime, Individu.bestChromo.fitness);
						System.out.println("Elpased Time : " + elapsedTime);
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}

				//Integer [] ch = {0, 5, 4, 3, 2, 7, 6, 1};
				//Individu.bestChromo.chromo = ch;
				ChessBoard.placeQueens(Individu.bestChromo.chromo);

				solPanel.removeAll();
				for(int i = 0; i < ChessBoard.size; i++) solPanel.add(new JLabel(i+"", SwingConstants.CENTER));
				for(int i = 0; i < ChessBoard.size; i++) solPanel.add(new JButton(Individu.bestChromo.chromo[i]+""));
				fitnessBtn.setText(Individu.bestChromo.fitness+"");
			}
		});
		//ajout �lement au panel
		paramPanel.setPreferredSize(new Dimension(210, 150));
		//paramPanel.setBorder(BorderFactory.createLineBorder(Color.black));
		configPanel.setPreferredSize(new Dimension(400, 200));
		resPanel.setPreferredSize(new Dimension(450, 550));
		gc.weightx = 0.1;
		gc.weighty = 0.1;
		for(int i = 0; i < menuGui.length; i++){
			gc.gridy = i;
			gc.gridx = 0;
			gc.anchor = GridBagConstraints.LAST_LINE_END;
			paramPanel.add(new JLabel(menuGui[i]+":"), gc);
			JTextField temp = new JTextField(4);
			fieldRef[i] = temp;
			gc.gridx = 1;
			gc.anchor = GridBagConstraints.LAST_LINE_START;
			paramPanel.add(temp, gc);
		}

		resPanel.add(new JLabel("Best Chromosom"));
		resPanel.add(solPanel);
		resPanel.add(ChessBoard.boardPanel);
		resPanel.add(nvcLbl);
		resPanel.add(fitnessBtn);
		configPanel.add(paramPanel);
		configPanel.add(runAlgoBtn);
		mainPanel.add(configPanel);
		mainPanel.add(resPanel);

		frame.setContentPane(mainPanel);
		frame.pack();
		frame.setVisible(true);
	}

}
