package csp;

public class Constraints {
	private static int sameDiag(int r1, int r2, int c1, int c2){
		int cv = 0;
		if(c2 > c1){
			if(r2 - r1 == c2 - c1) cv++;
		}else{
			if(r2 - r1 == c1 - c2) cv++;
		}
		return cv;
	}

	private static boolean sameColumn(int qx1, int qx2){
		return qx1 == qx2;
	}

	public static int checkQueens(int c1, int c2, int qx1, int qx2){
		int cv = 0;
		cv += sameDiag(c1,c2,qx1,qx2);
		if(sameColumn(qx1,qx2)) cv++;
		return cv;
	}
}
